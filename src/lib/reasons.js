export const REASONS = [
  {id: 1, name: 'Ненормативная лексика'},
  {id: 2, name: 'Нарушение условий использоввания'},
  {id: 3, name: 'Другое'}
];

export const CUSTOM_REASON = REASONS[2];
