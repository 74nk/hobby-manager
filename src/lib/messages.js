export const MESSAGE_TIMEOUT = 3000;
export const MESSAGE_ADDED = 'добавлено в ваши увлечения';
export const MESSAGE_UNABLE_ADD = 'не удалось добавить';
export const MESSAGE_REMOVED = 'удалено из ваших увлечений';
export const MESSAGE_UNABLE_REMOVE = 'не удалось удалить';
export const MESSAGE_COMPLAIN_SENT = 'жалоба отправлена';
export const MESSAGE_UNABLE_COMPLAIN = 'не удалось отправить жалобу';
