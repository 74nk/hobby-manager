// Dummy server api

let complaints = [];
let hobbies = [
    {
        "id": 1,
        "userId": 1,
        "content": "Хоккей"
    },
    {
        "id": 2,
        "userId": 1,
        "content": "Высокоточная вёрстка под старые версии Microsoft Internet Explorer, начиная с версии 5.01"
    },
    {
        "id": 3,
        "userId": 1,
        "content": "Бадминтон"
    },
    {
        "id": 4,
        "userId": 1,
        "content": "Плавание"
    },
    {
        "id": 5,
        "userId": 1,
        "content": "Шашки"
    },
    {
        "id": 6,
        "userId": 1,
        "content": "Борьба"
    },
    {
        "id": 7,
        "userId": 2,
        "content": "Баскетбол"
    },
    {
        "id": 8,
        "userId": 2,
        "content": "Нарезка Photoshop/Fireworks макетов на скорость, в экстримельных условиях, на природе"
    },
    {
        "id": 9,
        "userId": 2,
        "content": "Бадминтон"
    },
    {
        "id": 10,
        "userId": 2,
        "content": "Плавание"
    },
    {
        "id": 11,
        "userId": 2,
        "content": "Шашки"
    },
    {
        "id": 12,
        "userId": 2,
        "content": "Борьба"
    },
];

function getNextId() {
    return Math.max(...hobbies.map(it => it.id)) + 1
}

export function getCurrentUser() {
    return Promise.resolve({id: 1})
}

export function getHobbies() {
    return Promise.resolve(hobbies)
}

export function addHobby(hobby) {
    if(hobbies.some(it => it.userId === hobby.userId && it.content === hobby.content)) {
        return Promise.resolve(true)
            .then(() => {
                throw Error()
            })
    }

    hobbies.unshift(Object.assign(hobby, {
        id: getNextId()
    }));
    return Promise.resolve(true)
}

export function removeHobby(id) {
    hobbies = hobbies.filter(it => it.id !== id);
    return Promise.resolve(true)
}

export function shareHobby(id, userId) {
    const {content} = hobbies.find(it => it.id === id);
    return addHobby({content, userId});
}

export function sendComplaint(id, userId, reason) {
    complaints.push({id, userId, reason});
    return Promise.resolve(true)
}

