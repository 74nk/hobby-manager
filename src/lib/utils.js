export function getWordForm(number, [first, second, third]) {
  const lastNumber = number % 10;
  const lastTwoNumbers = number % 100;
  const notTeen = !(lastTwoNumbers > 10 && lastTwoNumbers < 20);

  return notTeen && lastNumber === 1
    ? first : (notTeen && lastNumber > 1 && lastNumber < 5 ? second : third);
}
